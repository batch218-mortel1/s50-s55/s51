/*
My Own Codes

import { Button, Row, Col, Card } from 'react-bootstrap';

export default function CourseCard() {
return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title>
                            <h4>Sample Course</h4>
                        </Card.Title>
                        <Card.Text>
                          <p>Description:</p>
                          <p>This is a sample course offering</p>
                          <p>Price:</p>
                          <p>PHP 40,000</p>
                        </Card.Text>
                        <Button variant="primary">Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
	)
}*/

import {useState} from 'react';
import { Button, Card } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard({course}) {

    const {name, description, price, id} = course;

    // count - getter
    // setCount - setter
    // useState(0) - useState(initialGetterValue)
    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    console.log(useState(0));

    // function that keeps track of the enrollees for a course
    function enroll() {
        setCount(count + 1);
        console.log('Enrollees: ' + count);

    // S51 ACTIVITY

        setSeats(seats - 1);
        console.log('Seats: ' + seats);

    // S51 ACTIVITY END

        if(seats === 1){
            alert("No more seats.");
            document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled' , true);
        }
    }


    // Checks to see if the data was successfully  passed
    // console.log(props);
    // Every component receives information in a form of an object
    // console.log(typeof props);

    return (
    <Card>
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>PhP {price}</Card.Text>
            <Card.Text>Enrollees: {count}</Card.Text>            
            <Card.Text>Seats: {seats}</Card.Text>            
            <Button id={'btn-enroll-' + id} className="bg-primary" onClick={enroll}>Enroll</Button>
        </Card.Body>
    </Card>
    )
}

// "proptypes" - are a good way of checking data type of information between components.
CourseCard.propTypes = {
    // "shape" method is used to check if prop object conforms to a specific "shape"
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    })
}
